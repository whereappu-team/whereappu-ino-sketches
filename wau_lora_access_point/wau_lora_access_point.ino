#include <SPI.h>
#include <RH_RF95.h>
#include <SoftwareSerial.h>

const bool DEBUG = false;
const String DEVICE_ID = "1"; // change accordingly
const String DELIMITER = "[SOS]";
const int LED_PIN = 10;

RH_RF95 rf95;

SoftwareSerial wifiSerial(4, 5);

void setup()
{
  Serial.begin(115200);

  while (!rf95.init()) { ; }
  rf95.setTxPower(14, false);
  rf95.setFrequency(915.0);
  rf95.setThisAddress(10);
  rf95.setHeaderFrom(10);
  rf95.setHeaderTo(1);
  if (DEBUG) {
    Serial.println("Ready to transmit LoRa packets!");
  }

  wifiSerial.begin(9600);
  wifiSerial.setTimeout(5);
  if (DEBUG) {
    Serial.println("Wifi connection is running!");
  }

  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, LOW);
}

void loop()
{
  if (wifiSerial.available() > 0) {

    String message = readWifiSerialMessage();

    if (DEBUG) {
      Serial.print("\r\nReceived: ");
      Serial.print(message);
    }

    int payloadIdx = message.indexOf(DELIMITER);
    if (payloadIdx >= 0) {
      String data = "[]" + DEVICE_ID + "|" + message.substring(payloadIdx + 5);

      if (DEBUG) {
        Serial.println("\r\n");
        Serial.println("Extracted payload");
        Serial.println("---------------------------------");
        Serial.print(data);
        Serial.println("\r\n=================================");
        Serial.println("Transmitting packet to LoRa...");
      }

      size_t blk_size = sizeof(uint8_t) * data.length();
      char const *c = data.c_str();
      rf95.send(c, blk_size);

      digitalWrite(LED_PIN, HIGH);

      rf95.waitPacketSent();
      delay(100);
      if (DEBUG) {
        Serial.println("Done");
      }

      digitalWrite(LED_PIN, LOW);
    }
  }
}

String  readWifiSerialMessage() {
  String str = "";
  while (wifiSerial.available() > 0) {
    str += wifiSerial.readStringUntil('\n');
  }
  return str;
}

