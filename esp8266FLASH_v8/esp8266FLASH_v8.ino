#include <ESP8266WiFi.h>

#define BAUD_RATE 9600
#define WITH_DELAY false
#define MAX_SRV_CLIENTS 8
#define SRV_CLIENT_TIMEOUT 5
#define SRV_PORT 80

const char *AP_SSID = "WhereAppU-WiFi";
const char RQ_STOP = ';';

WiFiServer server(SRV_PORT);
WiFiClient serverClients[MAX_SRV_CLIENTS];

void setup() {
  Serial.begin(BAUD_RATE);
  
  WiFi.softAP(AP_SSID);
  Serial.println("\n[WiFi AP completed setup]");
  
  server.begin();
  server.setNoDelay(WITH_DELAY);
  Serial.println("\n[WiFi server running]");
}

void loop() {
  uint8_t i;
  uint8_t j;
  String line = "";
  
  for (i = 0; i < MAX_SRV_CLIENTS; i++) {
    if (serverClients[i]) {
      if (serverClients[i].connected()) {
        // read incoming packets from connected clients
        while (serverClients[i].available() > 0) {
          line = serverClients[i].readStringUntil(RQ_STOP);
          Serial.print(line + "\n");
          Serial.flush();
        }
        delay(5);
        if (line.length() > 0) {
          serverClients[i].write("ok\n");
          serverClients[i].flush();
          delay(5);
          line = "";
        }
      } else {
        // close inactive connections
        serverClients[i].stop();
        Serial.println("\n[Client disconnected]");
      }
    }
  }
  
  //check if there are any new clients
  if (server.hasClient()) {
    for (j = 0; j < MAX_SRV_CLIENTS; j++) {
      //find free/disconnected spot
      if (!serverClients[j] || !serverClients[j].connected()) {
        Serial.println("\n[Client connected]");
        serverClients[j] = server.available();
        serverClients[j].setNoDelay(WITH_DELAY);
        serverClients[j].setTimeout(SRV_CLIENT_TIMEOUT);
        break;
      }
    }
    if ((j + 1) == MAX_SRV_CLIENTS) {
      //no free/disconnected spot so reject
      Serial.println("\n[Rejected client due to MAX_SRV_CLIENTS limit reached]");
      WiFiClient serverClient = server.available();
      serverClient.stop();
    }
  }
}

