# whereappu-ino-sketches

Arduino sketches used on our LoRa nodes, with both the sketch for Arduino UNO board and our custom firmware to flash the ESP-8266.  

Establishes a local WiFi access-point (WhereAppU-WiFi) through which our app can send SOS messages transmitted to gateway devices via LoRa network.
